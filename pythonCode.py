
#!/usr/bin/env python

from netmiko import ConnectHandler

iosv_CityCentre = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.235',
    'username': 'emma',
    'password': 'cisco',
}

iosv_CitySouth = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.236',
    'username': 'emma',
    'password': 'cisco',
}

iosv_CityNorth = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.237',
    'username': 'emma',
    'password': 'cisco',
}


with open('loopback') as f:
    lines = f.read().splitlines()
print (lines)


all_devices = [iosv_CitySouth, iosv_CityNorth]

for devices in all_devices:
    net_connect = ConnectHandler(**devices)
    output = net_connect.send_config_set(lines)
    output = net_connect.send_command('show ip int brief')
    print (output) 


with open('ospf') as f:
    lines = f.read().splitlines()
print (lines)

all_devices = [iosv_CityCentre]

for devices in all_devices:
    net_connect = ConnectHandler(**devices)
    output = net_connect.send_config_set(lines)
    output = net_connect.send_command('show ip protocol')
    print (output) 